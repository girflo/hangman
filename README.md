# Hangman

Language : Haskell // This software is a small hangman in console done in haskell.

## Game description

The project contain the source of the game in the src directory and the executable in the main directory.

The Dictionary file contain all words that can be asked by the game. The contain of this file can be edited in a editor but it's preferable to use the in-game function to add a word.

The goal a this game is to discover a word chosen by the script. The user need to input a letter each turn as a guess. If the letter is contained in the secret word the script will show the word with the letter(s) visible for the user otherwhise the user will lose one of its 8 life.

If the word is fully discovered the user win the game but if the number of live is 0 the user lose the game.

## Screenshots
**Main Menu :**

![hangman main menu](img/main_menu.png)

**Game :**

![hangman game](img/game.png)

**Win :**

![hangman win](img/win.png)

**Loose :**

![hangman loose](img/loose.png)
