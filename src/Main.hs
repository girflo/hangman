import Prelude hiding (catch)
import System.Directory
import Data.List
import Data.Char
import System.Exit
-- This one need to be installed via cabal for example
import System.Random
import Control.Exception
import System.IO
import System.IO.Error hiding (catch)

main :: IO ()
main = do
  -- Name of the dictionary
  let dictionaryPath = "Dictionary"
  -- Minimum content of the dictionary (just some random words)
  let basicWords = ["Serendipity", "Glitch", "Paradigms of programming", "Moiré", "Vexilology", "Flamingo", "Dune"]
  -- Create the dictionary if it doesn't exist and write the basicWord inside
  createDictionaryIfNotExists (dictionaryPath, basicWords)
  userWantedAction <- printMenu
  case userWantedAction of
    '0' -> exitSuccess
    '1' -> appendWordToDictionary dictionaryPath
    otherwise -> game dictionaryPath

-- PRINTING FUNCTIONS
-- Print each string given and return the string entered by the user
printAndReturnInput :: [String] -> IO String
printAndReturnInput printList = do
  mapM_(\x -> putStrLn x) printList
  choice <- getLine
  return choice

-- Show the menu until the user choose between one of the three options
printMenu :: IO Char
printMenu = do
  choice <- printAndReturnInput(["Enter 1 to add a word in the dictionary", "Enter 2 to play the hangman game", "Enter 0 to quit"])
  if elem (head choice) ['0', '1', '2']
    then return (head choice)
    else printMenu

-- Print the following informations before each guesses and grab the next char the user want to test
printGameInfoReturnUserInput :: Int -> IO String
printGameInfoReturnUserInput remainingTry = do
  putStr "Remaining try : "
  print remainingTry
  printAndReturnInput(["Enter the character that you want to test :"])

-- DICTIONARY HANDLING
-- Add all given words at the end of the dictionary
addWord :: (FilePath, [String]) -> IO ()
addWord (dictionaryPath, content) = do
  dictionary <- openFile dictionaryPath AppendMode
  mapM_(\x -> hPutStrLn dictionary (map toUpper x)) content
  hClose dictionary

-- Add a word at the end of the dictionary
appendWordToDictionary :: FilePath -> IO ()
appendWordToDictionary dictionaryPath = do
  word <- printAndReturnInput(["Enter the word that you want to add"])
  addWord (dictionaryPath, [word])

-- Check if the dictionary is present otherwise create it
createDictionaryIfNotExists :: (FilePath, [String]) -> IO ()
createDictionaryIfNotExists (dictionaryPath, content) = do
  dictionaryPresence <- doesFileExist dictionaryPath
  if dictionaryPresence
    then return ()
    else
      addWord (dictionaryPath, content)

-- Return the content of the dictionary
grabDictionary :: FilePath -> IO String
grabDictionary dictionaryPath = readFile dictionaryPath

-- Parse the content of the dictionary and return a list of string, each string is a word
parseDictionary :: String -> [String]
parseDictionary contentDictionary = lines contentDictionary

-- HANGMAN GAME
-- Game preparation
game :: FilePath -> IO()
game dictionaryPath = do
  -- Store the String without the IO tag
  contentDictionary <- grabDictionary dictionaryPath
  -- List of all word contain in the dictionary
  let parsedDictionary = parseDictionary contentDictionary
  -- Create a random number lower or equal to the length of the dictionary list
  randomWordIndex <- generateRandomNumber (length parsedDictionary)
  -- Grab a word in the dictionary at the random index create before
  -- Create a list of String where each string will be the user progress
  -- the first element is also created, it contain only underscores
  let chosenWord = grabChosenWord (randomWordIndex, parsedDictionary)
  let emptyWord = createUnderscroreString (chosenWord)
  result <- guesses(8, (chosenWord, emptyWord))
  if result == True
    then putStrLn "You win !"
    else putStrLn "Sorry, you lose. Maybe next time."

-- Return a string containing all letters found by the user
combineLastGuessWithCurrentState :: (String, String) -> String
combineLastGuessWithCurrentState (userCurrentState, lastGuess) = do
  let zippedResult = zip userCurrentState lastGuess
  map returnLetterOrUndescrore zippedResult

-- This function create a string full of underscore the same size as the word to guess
createUnderscroreString :: String -> String
createUnderscroreString chosenWord = map (\x -> if x == ' ' then x else '_') chosenWord

-- Generate a random number, this number is the index of the choosen word in the dictionary
generateRandomNumber :: Int -> IO Int
generateRandomNumber arraySize = randomRIO (0, arraySize - 1)

-- Return a word from the dictionary at a given index
grabChosenWord :: (Int, [String]) -> String
grabChosenWord (index, wordList) = wordList !! index

guesses :: (Int, (String, String)) -> IO Bool
guesses (remainingTry, (chosenWord, userCurrentState)) = do
  putStr"Current state : "
  putStrLn userCurrentState
  if remainingTry == 0
    then return False
    else do
      input <- printGameInfoReturnUserInput remainingTry
      let result = guessResult (chosenWord, toUpper (head input))
      let newCurrentState = combineLastGuessWithCurrentState(userCurrentState, result)
      if elem '_' newCurrentState
        then if newCurrentState == userCurrentState
          -- If the user correctly guess a character the remaining try remain the same
          then guesses (remainingTry - 1, (chosenWord, newCurrentState))
          else guesses (remainingTry, (chosenWord, newCurrentState))
        else return True

-- When the two char are the same return the charactere otherwise return undescrore
guessCharComparison :: (Char, Char) -> Char
guessCharComparison (x, y) = if x == y then x else '_'

-- Return the last user progress
guessResult :: (String, Char) -> String
guessResult (n, y) = map (\x -> guessCharComparison (x, y)) n

-- Return a lettre from the previous guesses or the last one if possible
returnLetterOrUndescrore :: (Char, Char) -> Char
returnLetterOrUndescrore (x, y) | x == '_' && y == '_' = '_'
                                | x == '_' = y
                                | otherwise = x
